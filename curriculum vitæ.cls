\NeedsTeXFormat{LaTeX2e}
\RequirePackage{expl3}
\ProvidesExplClass{curriculum vitæ}{2022/06/23}{0.1}{Curriculum vitæ de Benjamin Galliot}
\cs_set:Nn \__fontspec_load_external_fontoptions:Nn {}

\RequirePackage{l3keys2e}
\RequirePackage[dvipsnames, table, svgnames]{xcolor}

% Gestion des variables principales de document.

\keys_define:nn {document} {
    type .tl_set:N = \l_document_type,
    type .default:n = scrartcl,
    type .initial:n = scrartcl,
}

% Gestion des langues et polices.

\keys_define:nn {langues / source} {
    polices .code:n = { \keys_set:nn { langues / source / polices } {#1} },

    polices / rm .code:n = { \keys_set:nn { langues / source / polices / rm} {#1} },
    polices / rm / nom .tl_set:N = \l_langues_source_police_rm,
    polices / rm / nom .default:n = Gentium~Plus,
    polices / rm / nom .initial:n = Gentium~Plus,
    polices / rm / options .code:n = { \keys_set:nn { langues / source / polices / options} {#1} },
    polices / rm / options .tl_set:N = \l_langues_source_police_options_rm,
    polices / rm / options .default:n = {Script=Default, Language=Default, WordSpace = 1.2, StylisticSet={05}}, % Pour différencer a de ɑ en italique
    polices / rm / options .initial:n = {Script=Default, Language=Default, WordSpace = 1.2, StylisticSet={05}},

    polices / sf .code:n = { \keys_set:nn { langues / source / polices / sf} {#1} },
    polices / sf / nom .tl_set:N = \l_langues_source_police_sf,
    polices / sf / nom .default:n = Andika,
    polices / sf / nom .initial:n = Andika,
    polices / sf / options .code:n = { \keys_set:nn { langues / source / polices / options} {#1} },
    polices / sf / options .tl_set:N = \l_langues_source_police_options_sf,
    polices / sf / options .default:n = {Script=Default, Language=Default, WordSpace = 1.2, StylisticSet={05}}, % À éviter pour l’API (a toujours écrit ɑ)
    polices / sf / options .initial:n = {Script=Default, Language=Default, WordSpace = 1.2, StylisticSet={05}},

    polices / tt .code:n = { \keys_set:nn { langues / source / polices / tt} {#1} },
    polices / tt / nom .tl_set:N = \l_langues_source_police_tt,
    polices / tt / nom .default:n = Courier~New,
    polices / tt / nom .initial:n = Courier~New,
    polices / tt / options .code:n = { \keys_set:nn { langues / source / polices / options} {#1} },
    polices / tt / options .tl_set:N = \l_langues_source_police_options_tt,
    polices / tt / options .default:n = {Script=Default, Language=Default, WordSpace = 1.2},
    polices / tt / options .initial:n = {Script=Default, Language=Default, WordSpace = 1.2},
}

\keys_define:nn {langues / api} {
    polices .code:n = { \keys_set:nn { langues / api / polices } {#1} },

    polices / rm .code:n = { \keys_set:nn { langues / api / polices / rm} {#1} },
    polices / rm / nom .tl_set:N = \l_langues_api_police_rm,
    polices / rm / nom .default:n = Gentium~Plus,
    polices / rm / nom .initial:n = Gentium~Plus,
    polices / rm / options .code:n = { \keys_set:nn { langues / api / polices / options} {#1} },
    polices / rm / options .tl_set:N = \l_langues_api_police_options_rm,
    polices / rm / options .default:n = {Script=Default, Language=Default, WordSpace = 1.2, StylisticSet={05}}, % Pour différencer a de ɑ en italique
    polices / rm / options .initial:n = {Script=Default, Language=Default, WordSpace = 1.2, StylisticSet={05}},

    polices / sf .code:n = { \keys_set:nn { langues / api / polices / sf} {#1} },
    polices / sf / nom .tl_set:N = \l_langues_api_police_sf,
    polices / sf / nom .default:n = Andika,
    polices / sf / nom .initial:n = Andika,
    polices / sf / options .code:n = { \keys_set:nn { langues / api / polices / options} {#1} },
    polices / sf / options .tl_set:N = \l_langues_api_police_options_sf,
    polices / sf / options .default:n = {Script=Default, Language=Default, WordSpace = 1.2, StylisticSet={05}}, % À éviter pour l’API (a toujours écrit ɑ)
    polices / sf / options .initial:n = {Script=Default, Language=Default, WordSpace = 1.2, StylisticSet={05}},

    polices / tt .code:n = { \keys_set:nn { langues / api / polices / tt} {#1} },
    polices / tt / nom .tl_set:N = \l_langues_api_police_tt,
    polices / tt / nom .default:n = Courier~New,
    polices / tt / nom .initial:n = Courier~New,
    polices / tt / options .code:n = { \keys_set:nn { langues / api / polices / options} {#1} },
    polices / tt / options .tl_set:N = \l_langues_api_police_options_tt,
    polices / tt / options .default:n = {Script=Default, Language=Default, WordSpace = 1.2},
    polices / tt / options .initial:n = {Script=Default, Language=Default, WordSpace = 1.2},
}

\keys_define:nn {langues / symbolique} {
    polices .code:n = { \keys_set:nn { langues / symbolique / polices } {#1} },

    polices / rm .code:n = { \keys_set:nn { langues / symbolique / polices / rm} {#1} },
    polices / rm / nom .tl_set:N = \l_langues_symbolique_police_rm,
    polices / rm / nom .default:n = Symbola,
    polices / rm / nom .initial:n = Symbola,
    polices / rm / options .code:n = { \keys_set:nn { langues / symbolique / polices / options} {#1} },
    polices / rm / options .tl_set:N = \l_langues_symbolique_police_options_rm,
    polices / rm / options .default:n = {Script=Default, Language=Default},
    polices / rm / options .initial:n = {Script=Default, Language=Default},

    polices / sf .code:n = { \keys_set:nn { langues / symbolique / polices / sf} {#1} },
    polices / sf / nom .tl_set:N = \l_langues_symbolique_police_sf,
    polices / sf / nom .default:n = Symbola,
    polices / sf / nom .initial:n = Symbola,
    polices / sf / options .code:n = { \keys_set:nn { langues / symbolique / polices / options} {#1} },
    polices / sf / options .tl_set:N = \l_langues_symbolique_police_options_sf,
    polices / sf / options .default:n = {Script=Default, Language=Default},
    polices / sf / options .initial:n = {Script=Default, Language=Default},

    polices / tt .code:n = { \keys_set:nn { langues / symbolique / polices / tt} {#1} },
    polices / tt / nom .tl_set:N = \l_langues_symbolique_police_tt,
    polices / tt / nom .default:n = Symbola,
    polices / tt / nom .initial:n = Symbola,
    polices / tt / options .code:n = { \keys_set:nn { langues / symbolique / polices / options} {#1} },
    polices / tt / options .tl_set:N = \l_langues_symbolique_police_options_tt,
    polices / tt / options .default:n = {Script=Default, Language=Default},
    polices / tt / options .initial:n = {Script=Default, Language=Default},
}

\keys_define:nn {langues / fra} {
    polices .code:n = { \keys_set:nn { langues / fra / polices } {#1} },

    polices / rm .code:n = { \keys_set:nn { langues / fra / polices / rm} {#1} },
    polices / rm / nom .tl_set:N = \l_langues_fra_police_rm,
    polices / rm / nom .default:n = Cormorant~Garamond,
    polices / rm / nom .initial:n = Cormorant,
    polices / rm / options .code:n = { \keys_set:nn { langues / fra / polices / options} {#1} },
    polices / rm / options .tl_set:N = \l_langues_fra_police_options_rm,
    polices / rm / options .default:n = {Script=Latin, Language=French, StylisticSet = 1, Ligatures = {Required, TeX, Common, Contextual, Rare, Historic}, Style = Historic, Numbers = OldStyle},
    polices / rm / options .initial:n = {Script=Latin, Language=French, StylisticSet = 1, Ligatures = {Required, TeX, Common, Contextual, Rare, Historic}, Style = Historic, Numbers = OldStyle},

    polices / sf .code:n = { \keys_set:nn { langues / fra / polices / sf} {#1} },
    polices / sf / nom .tl_set:N = \l_langues_fra_police_sf,
    polices / sf / nom .default:n = Ysabeau~Office,
    polices / sf / nom .initial:n = Ysabeau,
    polices / sf / options .code:n = { \keys_set:nn { langues / fra / polices / options} {#1} },
    polices / sf / options .tl_set:N = \l_langues_fra_police_options_sf,
    polices / sf / options .default:n = {Script=Latin, Language=French, StylisticSet = 1, Ligatures = {Required, TeX, Common, Contextual, Rare, Historic}, Style = Historic, Numbers = OldStyle},
    polices / sf / options .initial:n = {Script=Latin, Language=French, StylisticSet = 1, Ligatures = {Required, TeX, Common, Contextual, Rare, Historic}, Style = Historic, Numbers = OldStyle},

    polices / tt .code:n = { \keys_set:nn { langues / fra / polices / tt} {#1} },
    polices / tt / nom .tl_set:N = \l_langues_fra_police_tt,
    polices / tt / nom .default:n = Courier~New,
    polices / tt / nom .initial:n = Courier~New,
    polices / tt / options .code:n = { \keys_set:nn { langues / fra / polices / options} {#1} },
    polices / tt / options .tl_set:N = \l_langues_fra_police_options_tt,
    polices / tt / options .default:n = {Script=Latin, Language=French},
    polices / tt / options .initial:n = {Script=Latin, Language=French},
}

\keys_define:nn {langues / eng} {
    polices .code:n = { \keys_set:nn { langues / eng / polices } {#1} },

    polices / rm .code:n = { \keys_set:nn { langues / eng / polices / rm} {#1} },
    polices / rm / nom .tl_set:N = \l_langues_eng_police_rm,
    polices / rm / nom .default:n = Cormorant~Garamond,
    polices / rm / nom .initial:n = Cormorant,
    polices / rm / options .code:n = { \keys_set:nn { langues / eng / polices / options} {#1} },
    polices / rm / options .tl_set:N = \l_langues_eng_police_options_rm,
    polices / rm / options .default:n = {Script=Latin, Language=English, StylisticSet = 1, Ligatures = {Required, TeX, Common, Contextual, Rare, Historic}, Style = Historic, Numbers = OldStyle},
    polices / rm / options .initial:n = {Script=Latin, Language=English, StylisticSet = 1, Ligatures = {Required, TeX, Common, Contextual, Rare, Historic}, Style = Historic, Numbers = OldStyle},

    polices / sf .code:n = { \keys_set:nn { langues / eng / polices / sf} {#1} },
    polices / sf / nom .tl_set:N = \l_langues_eng_police_sf,
    polices / sf / nom .default:n = Ysabeau~Office,
    polices / sf / nom .initial:n = Ysabeau,
    polices / sf / options .code:n = { \keys_set:nn { langues / eng / polices / options} {#1} },
    polices / sf / options .tl_set:N = \l_langues_eng_police_options_sf,
    polices / sf / options .default:n = {Script=Latin, Language=English, StylisticSet = 1, Ligatures = {Required, TeX, Common, Contextual, Rare, Historic}, Style = Historic, Numbers = OldStyle},
    polices / sf / options .initial:n = {Script=Latin, Language=English, StylisticSet = 1, Ligatures = {Required, TeX, Common, Contextual, Rare, Historic}, Style = Historic, Numbers = OldStyle},

    polices / tt .code:n = { \keys_set:nn { langues / eng / polices / tt} {#1} },
    polices / tt / nom .tl_set:N = \l_langues_eng_police_tt,
    polices / tt / nom .default:n = Courier~New,
    polices / tt / nom .initial:n = Courier~New,
    polices / tt / options .code:n = { \keys_set:nn { langues / eng / polices / options} {#1} },
    polices / tt / options .tl_set:N = \l_langues_eng_police_options_tt,
    polices / tt / options .default:n = {Script=Latin, Language=English},
    polices / tt / options .initial:n = {Script=Latin, Language=English,},
}

\keys_define:nn {langues / cmn-CN} {
    polices .code:n = { \keys_set:nn { langues / cmn-CN / polices } {#1} },

    polices / rm .code:n = { \keys_set:nn { langues / cmn-CN / polices / rm} {#1} },
    polices / rm / nom .tl_set:N = \l_langues_cmncn_police_rm,
    polices / rm / nom .default:n = AR~PL~UKai~CN,
    polices / rm / nom .initial:n = Noto~Serif~CJK~SC,
    polices / rm / options .code:n = { \keys_set:nn { langues / cmn-CN / polices / options} {#1} },
    polices / rm / options .tl_set:N = \l_langues_cmncn_police_options_rm,
    polices / rm / options .default:n = {Script=CJK, Language=Chinese~Simplified, CJKShape=Simplified},
    polices / rm / options .initial:n = {Script=CJK, Language=Chinese~Simplified, CJKShape=Simplified},

    polices / sf .code:n = { \keys_set:nn { langues / cmn-CN / polices / sf} {#1} },
    polices / sf / nom .tl_set:N = \l_langues_cmncn_police_sf,
    polices / sf / nom .default:n = Noto~Sans~CJK~SC,
    polices / sf / nom .initial:n = Noto~Sans~CJK~SC,
    polices / sf / options .code:n = { \keys_set:nn { langues / cmn-CN / polices / options} {#1} },
    polices / sf / options .tl_set:N = \l_langues_cmncn_police_options_sf,
    polices / sf / options .default:n = {Script=CJK, Language=Chinese~Simplified, CJKShape=Simplified},
    polices / sf / options .initial:n = {Script=CJK, Language=Chinese~Simplified, CJKShape=Simplified},

    polices / tt .code:n = { \keys_set:nn { langues / cmn-CN / polices / tt} {#1} },
    polices / tt / nom .tl_set:N = \l_langues_cmncn_police_tt,
    polices / tt / nom .default:n = Noto~Sans~Mono~CJK~SC,
    polices / tt / nom .initial:n = Noto~Sans~Mono~CJK~SC,
    polices / tt / options .code:n = { \keys_set:nn { langues / cmn-CN / polices / options} {#1} },
    polices / tt / options .tl_set:N = \l_langues_cmncn_police_options_tt,
    polices / tt / options .default:n = {Script=CJK, Language=Chinese~Simplified, CJKShape=Simplified},
    polices / tt / options .initial:n = {Script=CJK, Language=Chinese~Simplified, CJKShape=Simplified},
}

\keys_define:nn {langues / cmn-HK} {
    polices .code:n = { \keys_set:nn { langues / cmn-HK / polices } {#1} },

    polices / rm .code:n = { \keys_set:nn { langues / cmn-HK / polices / rm} {#1} },
    polices / rm / nom .tl_set:N = \l_langues_cmnhk_police_rm,
    polices / rm / nom .default:n = Noto~Serif~CJK~HK,
    polices / rm / nom .initial:n = Noto~Serif~CJK~HK,
    polices / rm / options .code:n = { \keys_set:nn { langues / cmn-HK / polices / options} {#1} },
    polices / rm / options .tl_set:N = \l_langues_cmnhk_police_options_rm,
    polices / rm / options .default:n = {Script=CJK, Language=Chinese~Hong~Kong, CJKShape=Traditional},
    polices / rm / options .initial:n = {Script=CJK, Language=Chinese~Hong~Kong, CJKShape=Traditional},

    polices / sf .code:n = { \keys_set:nn { langues / cmn-HK / polices / sf} {#1} },
    polices / sf / nom .tl_set:N = \l_langues_cmnhk_police_sf,
    polices / sf / nom .default:n = Noto~Sans~CJK~HK,
    polices / sf / nom .initial:n = Noto~Sans~CJK~HK,
    polices / sf / options .code:n = { \keys_set:nn { langues / cmn-HK / polices / options} {#1} },
    polices / sf / options .tl_set:N = \l_langues_cmnhk_police_options_sf,
    polices / sf / options .default:n = {Script=CJK, Language=Chinese~Hong~Kong, CJKShape=Traditional},
    polices / sf / options .initial:n = {Script=CJK, Language=Chinese~Hong~Kong, CJKShape=Traditional},

    polices / tt .code:n = { \keys_set:nn { langues / cmn-HK / polices / tt} {#1} },
    polices / tt / nom .tl_set:N = \l_langues_cmnhk_police_tt,
    polices / tt / nom .default:n = Noto~Sans~Mono~CJK~HK,
    polices / tt / nom .initial:n = Noto~Sans~Mono~CJK~HK,
    polices / tt / options .code:n = { \keys_set:nn { langues / cmn-HK / polices / options} {#1} },
    polices / tt / options .tl_set:N = \l_langues_cmnhk_police_options_tt,
    polices / tt / options .default:n = {Script=CJK, Language=Chinese~Hong~Kong, CJKShape=Traditional},
    polices / tt / options .initial:n = {Script=CJK, Language=Chinese~Hong~Kong, CJKShape=Traditional},
}

\keys_define:nn {langues / cmn-TW} {
    polices .code:n = { \keys_set:nn { langues / cmn-TW / polices } {#1} },

    polices / rm .code:n = { \keys_set:nn { langues / cmn-TW / polices / rm} {#1} },
    polices / rm / nom .tl_set:N = \l_langues_cmntw_police_rm,
    polices / rm / nom .default:n = Noto~Serif~CJK~TC,
    polices / rm / nom .initial:n = Noto~Serif~CJK~TC,
    polices / rm / options .code:n = { \keys_set:nn { langues / cmn-TW / polices / options} {#1} },
    polices / rm / options .tl_set:N = \l_langues_cmntw_police_options_rm,
    polices / rm / options .default:n = {Script=CJK, Language=Chinese~Traditional, CJKShape=Traditional},
    polices / rm / options .initial:n = {Script=CJK, Language=Chinese~Traditional, CJKShape=Traditional},

    polices / sf .code:n = { \keys_set:nn { langues / cmn-TW / polices / sf} {#1} },
    polices / sf / nom .tl_set:N = \l_langues_cmntw_police_sf,
    polices / sf / nom .default:n = Noto~Sans~CJK~TC,
    polices / sf / nom .initial:n = Noto~Sans~CJK~TC,
    polices / sf / options .code:n = { \keys_set:nn { langues / cmn-TW / polices / options} {#1} },
    polices / sf / options .tl_set:N = \l_langues_cmntw_police_options_sf,
    polices / sf / options .default:n = {Script=CJK, Language=Chinese~Traditional, CJKShape=Traditional},
    polices / sf / options .initial:n = {Script=CJK, Language=Chinese~Traditional, CJKShape=Traditional},

    polices / tt .code:n = { \keys_set:nn { langues / cmn-TW / polices / tt} {#1} },
    polices / tt / nom .tl_set:N = \l_langues_cmntw_police_tt,
    polices / tt / nom .default:n = Noto~Sans~Mono~CJK~TC,
    polices / tt / nom .initial:n = Noto~Sans~Mono~CJK~TC,
    polices / tt / options .code:n = { \keys_set:nn { langues / cmn-TW / polices / options} {#1} },
    polices / tt / options .tl_set:N = \l_langues_cmntw_police_options_tt,
    polices / tt / options .default:n = {Script=CJK, Language=Chinese~Traditional, CJKShape=Traditional},
    polices / tt / options .initial:n = {Script=CJK, Language=Chinese~Traditional, CJKShape=Traditional},
}

\keys_define:nn {langues / jpn} {
    polices .code:n = { \keys_set:nn { langues / jpn / polices } {#1} },

    polices / rm .code:n = { \keys_set:nn { langues / jpn / polices / rm} {#1} },
    polices / rm / nom .tl_set:N = \l_langues_jpn_police_rm,
    polices / rm / nom .default:n = Noto~Serif~CJK~JP,
    polices / rm / nom .initial:n = Noto~Serif~CJK~JP,
    polices / rm / options .code:n = { \keys_set:nn { langues / jpn / polices / options} {#1} },
    polices / rm / options .tl_set:N = \l_langues_jpn_police_options_rm,
    polices / rm / options .default:n = {Script=CJK, Language=Japanese, CJKShape=NLC},
    polices / rm / options .initial:n = {Script=CJK, Language=Japanese, CJKShape=NLC},

    polices / sf .code:n = { \keys_set:nn { langues / jpn / polices / sf} {#1} },
    polices / sf / nom .tl_set:N = \l_langues_jpn_police_sf,
    polices / sf / nom .default:n = Noto~Sans~CJK~JP,
    polices / sf / nom .initial:n = Noto~Sans~CJK~JP,
    polices / sf / options .code:n = { \keys_set:nn { langues / jpn / polices / options} {#1} },
    polices / sf / options .tl_set:N = \l_langues_jpn_police_options_sf,
    polices / sf / options .default:n = {Script=CJK, Language=Japanese, CJKShape=NLC},
    polices / sf / options .initial:n = {Script=CJK, Language=Japanese, CJKShape=NLC},

    polices / tt .code:n = { \keys_set:nn { langues / jpn / polices / tt} {#1} },
    polices / tt / nom .tl_set:N = \l_langues_jpn_police_tt,
    polices / tt / nom .default:n = Noto~Sans~Mono~CJK~JP,
    polices / tt / nom .initial:n = Noto~Sans~Mono~CJK~JP,
    polices / tt / options .code:n = { \keys_set:nn { langues / jpn / polices / options} {#1} },
    polices / tt / options .tl_set:N = \l_langues_jpn_police_options_tt,
    polices / tt / options .default:n = {Script=CJK, Language=Japanese, CJKShape=NLC},
    polices / tt / options .initial:n = {Script=CJK, Language=Japanese, CJKShape=NLC},
}

\keys_define:nn {langues / kor} {
    polices .code:n = { \keys_set:nn { langues / kor / polices } {#1} },

    polices / rm .code:n = { \keys_set:nn { langues / kor / polices / rm} {#1} },
    polices / rm / nom .tl_set:N = \l_langues_kor_police_rm,
    polices / rm / nom .default:n = Noto~Serif~CJK~KR,
    polices / rm / nom .initial:n = Noto~Serif~CJK~KR,
    polices / rm / options .code:n = { \keys_set:nn { langues / kor / polices / options} {#1} },
    polices / rm / options .tl_set:N = \l_langues_kor_police_options_rm,
    polices / rm / options .default:n = {Script=CJK, Language=Korean},
    polices / rm / options .initial:n = {Script=CJK, Language=Korean},

    polices / sf .code:n = { \keys_set:nn { langues / kor / polices / sf} {#1} },
    polices / sf / nom .tl_set:N = \l_langues_kor_police_sf,
    polices / sf / nom .default:n = Noto~Sans~CJK~KR,
    polices / sf / nom .initial:n = Noto~Sans~CJK~KR,
    polices / sf / options .code:n = { \keys_set:nn { langues / kor / polices / options} {#1} },
    polices / sf / options .tl_set:N = \l_langues_kor_police_options_sf,
    polices / sf / options .default:n = {Script=CJK, Language=Korean},
    polices / sf / options .initial:n = {Script=CJK, Language=Korean},

    polices / tt .code:n = { \keys_set:nn { langues / kor / polices / tt} {#1} },
    polices / tt / nom .tl_set:N = \l_langues_kor_police_tt,
    polices / tt / nom .default:n = Noto~Sans~Mono~CJK~KR,
    polices / tt / nom .initial:n = Noto~Sans~Mono~CJK~KR,
    polices / tt / options .code:n = { \keys_set:nn { langues / kor / polices / options} {#1} },
    polices / tt / options .tl_set:N = \l_langues_kor_police_options_tt,
    polices / tt / options .default:n = {Script=CJK, Language=Korean},
    polices / tt / options .initial:n = {Script=CJK, Language=Korean},
}

\ProcessKeysOptions{document}
\ProcessKeysOptions{langues}

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{\l_document_type}}
\ProcessOptions\relax
\LoadClass{\l_document_type}

\DeclareDocumentCommand \latin {} {}

\DeclareDocumentCommand \ConfigurerLangue { o m o }
{
    \tl_if_novalue:nTF {#3} {} {
        \tl_set:Nn \code {#2} % À améliorer…
        \tl_if_eq:NNTF \code \l_langues_source {
            \keys_set:nn {langues / source}{#3}
        } {
            \keys_set:nn {langues / #2}{#3}
        }
    }
    \str_case_e:nnTF {#2} {
        {symbolique} {
            \configurer_langue{symbolique}{fr}
        }
        {fra} {
            \tl_set:Nn \options {#1} % À améliorer…
            \tl_if_eq:NnTF \options {main} {
                \RequirePackage[french]{babel} % \frenchsetup a besoin pour exister d’avoir la langue « french » et ne se satisfait pas de « fra ».
            } {
                \RequirePackage{babel}
            }
            \ConfigurerLangue{symbolique}
            \configurer_langue{fra}{fr}[#1]
            \RequirePackage{impnattypo}
            \frenchsetup{og=«, fg=», ItemLabels=\textesymbolique{\footnotesize ☯}, UnicodeNoBreakSpaces} % Saisie directe des guillemets, énumérations modifiées et codage réel des espaces spéciales.
            \setlength{\listindentFB}{0pt} % Évite un retrait au commencement des listes.
        }
        {eng} {
            \configurer_langue{eng}{en}[#1]
        }
        {cmn-CN} {
            \configurer_langue_cjc{cmncn}{zh-Hans}
        }
        {cmn-HK} {
            \configurer_langue_cjc{cmnhk}{zh-Hant}
        }
        {cmn-TW} {
            \configurer_langue_cjc{cmntw}{zh-Hant}
        }
        {jpn} {
            \configurer_langue_cjc{jpn}{ja}
        }
        {kor} {
            \configurer_langue_cjc{kor}{ko}
        }
        {api} {
            \configurer_langue{api}{fr}
        }
    } {} {
        \msg_new:nnnn { Curriculum~vitæ } { Erreur~de~langue } { Langue~#2~inconnue. } { Liste~des~langues~disponibles~actuellement~:~fra,~cmn. }
        \bool_set_true:N \l_tmpa_bool
        \bool_if:NTF \l_tmpa_bool {
            \msg_error:nn { Curriculum~vitæ } { Erreur~de~langue }
        }
    }
}

\DeclareDocumentCommand \configurer_langue_source {} {
    \babelprovide[hyphenrules=+]{\l_langues_source}
    \babelfont[\l_langues_source]{rm}[\l_langues_source_police_options_rm]{\l_langues_source_police_rm}
    \babelfont[\l_langues_source]{sf}[\l_langues_source_police_options_sf]{\l_langues_source_police_sf}
    \babelfont[\l_langues_source]{tt}[\l_langues_source_police_options_tt]{\l_langues_source_police_tt}
    \cs_new:cpn { police\l_langues_source } { \selectlanguage{\l_langues_source} }
    \cs_new:cpn { texte\l_langues_source } ##1 { \foreignlanguage{\l_langues_source}{##1} }
}

\DeclareDocumentCommand \configurer_langue { m m o } {
    \RequirePackage{fontspec}
    \babelprovide[import=#2, #3]{#1}
    \babelfont[#1]{rm}[\use:c {l_langues_#1_police_options_rm}]{\use:c {l_langues_#1_police_rm}}
    \babelfont[#1]{sf}[\use:c {l_langues_#1_police_options_sf}]{\use:c {l_langues_#1_police_sf}}
    \babelfont[#1]{tt}[\use:c {l_langues_#1_police_options_tt}]{\use:c {l_langues_#1_police_tt}}
    \cs_new:cpn { police#1 } { \selectlanguage{#1} }
    \cs_new:cpn { texte#1 } ##1 { \foreignlanguage{#1}{##1} }
}

\DeclareDocumentCommand \configurer_langue_cjc { m m } {
    \RequirePackage{ctex}
    \babelprovide[import=#2]{#1}
    \babelfont[#1]{rm}[\use:c {l_langues_#1_police_options_rm}]{\use:c {l_langues_#1_police_rm}}
    \babelfont[#1]{sf}[\use:c {l_langues_#1_police_options_sf}]{\use:c {l_langues_#1_police_sf}}
    \babelfont[#1]{tt}[\use:c {l_langues_#1_police_options_tt}]{\use:c {l_langues_#1_police_tt}}
    \setCJKfamilyfont{#1rm}[\use:c {l_langues_#1_police_options_rm}]{\use:c {l_langues_#1_police_rm}}
    \setCJKfamilyfont{#1sf}[\use:c {l_langues_#1_police_options_sf}]{\use:c {l_langues_#1_police_sf}}
    \setCJKfamilyfont{#1tt}[\use:c {l_langues_#1_police_options_tt}]{\use:c {l_langues_#1_police_tt}}
    \expandafter\addto\csname extras #1\endcsname{ % À améliorer plus tard…
        \RenewDocumentCommand {\CJKrmdefault} {} {#1rm}
        \RenewDocumentCommand {\CJKsfdefault} {} {#1sf}
        \RenewDocumentCommand {\CJKsfdefault} {} {#1tt}
    }
    \DeclareDocumentCommand {\cjc} {} {\ltjsetparameter{jacharrange={-1, +2, +3, -4, -5, +6, +7, -8, +9}}}
    \RenewDocumentCommand {\latin} {} {\ltjsetparameter{jacharrange={-1, -2, -3, -4, -5, +6, +7, -8, -9}}}
    \cs_new:cpn { police#1 } { \selectlanguage{#1}\cjc }
    \cs_new:cpn { texte#1 } ##1 { \foreignlanguage{#1}{\cjc##1} }

    \ajouter_ruby
}

\DeclareDocumentCommand \ajouter_ruby {} {
    % \RequirePackage{ruby} % Problèmes de polices…
    % \RequirePackage{pxrubrica} % Complexe avec les subdivisions
    \RequirePackage{luatexja-ruby} % Petit problème d’alignement par défaut (cf https://osdn.net/projects/luatex-ja/ticket/44916)
    \DeclareDocumentCommand \blocruby { O{jpn} O{fra} m m } {
        \use:c {texte##1}{\ltjruby[intergap=0.1, fontcmd=\use:c {police##2}]{##3}{\smash{##4}}}
    }
    \RenewDocumentCommand {\latin} {} {\ltjsetparameter{jacharrange={-1, -2, -3, -4, -5, +6, +7, -8, -9}}}
}

\DeclareDocumentCommand \AjouterRuby {} {\ajouter_ruby}

\DeclareDocumentCommand \ConfigurerArrièreplan { m O{0.15} O{1.6}}
{
    \RequirePackage{background} % Pour l’arrière-plan.
    \backgroundsetup{
      scale=1,
      color=black,
      opacity=0.5,
      angle=0,
      contents={%
      \begin{tikzpicture}[remember~picture, overlay]
        \node[opacity=#2, scale=#3] at (current~page.center){\includegraphics[width=\paperwidth,height=\paperheight]{#1}};
      \end{tikzpicture}
        }%
    }
}

% Format (dimensions générales).
\RequirePackage[a4paper, left=20mm, right=20mm, top=18mm, bottom=18mm, footskip=1cm, nomarginpar, heightrounded]{geometry}

% Changement en cours de page des marges.
\RequirePackage{changepage}

% Commandes pour les logos Xe/LuaLaTeX.
\RequirePackage{metalogo}

% Liens.
\RequirePackage[bookmarks=true, bookmarksnumbered=true, bookmarksopenlevel=5, bookmarksdepth=5, colorlinks=true, linkcolor=blue, urlcolor=blue!50!black, citecolor=blue]{hyperref}
\RequirePackage[depth=5, numbered]{bookmark}

% Bibliographie avec gestion multilingue.
\RequirePackage[style=authoryear, useprefix, maxcitenames=2, maxbibnames=30, autolang=other, language=auto, clearlang=false, backend=biber]{biblatex}

% Coupure automatique des liens longs.
\RequirePackage{xurl}

% Guillemets multilingues dans la bibliographie.
\RequirePackage[autostyle]{csquotes}

% La police gère moyennement bien le crénage du caractère « … » (pour avoir des points de suspension serrés même en utilisant ce caractère).
\RequirePackage{newunicodechar}
\newunicodechar{…}{...}

% Gestion fine des caractères.
% \usepackage{microtype}

% Interlignage.
% \usepackage{setspace}
% \spacing{1.5}

% Accès au numéro de dernière page.
\RequirePackage{lastpage}

% En-tête et pied de page.
% \RequirePackage{fancyhdr}
\RequirePackage{scrlayer-scrpage} % Mieux avec KOMA.

% Pied de page.
\clearpairofpagestyles
\cfoot{\scriptsize \thepage/\pageref{LastPage}}
\ofoot{\tiny \textcolor{gray!50!white}{version~du~\today}}

% Commandes de date.
% \usepackage[ddmmyyyy]{datetime}

% Cadres évolués
\RequirePackage{tcolorbox}

% Style discret (anti-emphase).
\DeclareDocumentCommand \déemph { m } {\textcolor{black!60}{#1}}

% Basculement des références anglaises en style français (noms en petites capitales) dans la bibliographie.
\DefineBibliographyExtras{english}{
    \RenewDocumentCommand {\mkbibnamefamily} { m } {\textsc{\textnohyphenation{#1}}}
}

% Polices spécifiques dans la bibliographie.
\DefineBibliographyExtras{french}{\policefra}
\DefineBibliographyExtras{english}{\policeeng}

%our corriger l’ajout superflu d’une espace après le guillemet ouvrant (voir https://github.com/josephwright/csquotes/issues/48), ce qui arrive si csquotes ajoute les guillemets automatiquement dans la bibliographie.
% \RenewDocumentCommand {\@frenchquotespace} {} {}

% Pour diminuer la taille de la bibliographie.
\RenewDocumentCommand {\bibfont} {} {\footnotesize}

% Pour éviter une ligne vide après certaines entrées bibliographiques…
\renewbibmacro{finentry}{\par}

% Pour avoir les bons guillemets en faisant le lien avec les codes de langue utilisé dans cette classe.
\DeclareQuoteAlias{french}{fra}
\DeclareQuoteAlias{english}{eng}

% Pour éviter l’incompatibilité entre csquotes et babel-fr avec la saisie directe des guillemets.
\DeclareQuoteStyle[quotes]{french}
  {\og}
  {\fg}
  {\textquotedblleft}
  {\textquotedblright}

% Pour cacher les dates de dernier visionnage des liens.
\AtEveryBibitem{%
\ifentrytype{inproceedings}{
  \clearfield{urlyear}%
}{}
\ifentrytype{book}{
  \clearfield{urlyear}%
}{}
\ifentrytype{misc}{
  \clearfield{urlyear}%
}{}
\ifentrytype{article}{
  \clearfield{urlyear}%
}{}
\ifentrytype{online}{
  \clearfield{urlyear}%
}{}
\ifentrytype{report}{
  \clearfield{urlyear}%
}{}
\ifentrytype{thesis}{
  \clearfield{urlyear}%
}{}
}

% Filtres pour la bibliographie partitionnée.
\defbibfilter{logiciels}{
    type=software~or~keyword=gitlab
}
\defbibfilter{dictionnaires}{
    keyword=lexika
}
\defbibfilter{corpus}{
    keyword=outilspangloss
}
\defbibfilter{articles}{
    type=inproceedings~or~type=article~and~not~keyword=remerciements~and~not~keyword=lexika~and~not~keyword=outilspangloss
}
\defbibfilter{rapports}{
    type=report
}
\defbibfilter{remerciements}{
    keyword=remerciements
}

\hypersetup{
    pdfcreator = {\LuaLaTeX{}},
    pdfauthor = {Benjamin~Galliot},
}

\RequirePackage{paracol}
\RequirePackage{graphicx}
\RequirePackage{hyphenat}
\RequirePackage{ragged2e}

\tl_set:Nn \largeurmarge {2cm}
\tl_set:Nn \longueurintercolonne {1em}
\tl_set:Nn \couleurprincipale {violet}
\tl_set:Nn \couleursecondaire {green}

\DeclareDocumentCommand \stylesection { m } {\textcolor{\couleurprincipale}{\Large\bfseries\upshape #1}}
\DeclareDocumentCommand \stylesoussection { m } {\textcolor{\couleurprincipale}{\large\bfseries\upshape #1}}
\DeclareDocumentCommand \ligne { o m } {\leaders\hbox{\rule[#1]{1pt}{#2}}\hfill}
% Pour éviter une ligne ou une espace vide avant une liste en début de bloc…
\DeclareDocumentCommand \findeligne {} {\par\noindent\ignorespaces}

% Cadres.
\newtcolorbox{cadreaccroche}[1][opacityback=0.5]{
    sharp~corners=uphill,
    arc=15pt,
    colback=\couleurprincipale!5!white,
    colframe=\couleurprincipale,
    boxrule=0.8pt,
    frame~empty,
    #1
}

\DeclareDocumentCommand \sectioncv { m m o o } {
    \medskip
    \setcolumnwidth{\largeurmarge}
    \tl_if_novalue:nTF {#3} {} {\phantomsection\label{#3}{}}
    \begin{paracol}{2}
        \noindent
        \includegraphics[keepaspectratio, height=0.6\baselineskip]{#2.png}
        \hspace{\longueurintercolonne}
        {\color{\couleurprincipale!50!white}\ligne[0.2\baselineskip]{3pt}}
        \switchcolumn
        \phantomsection
        \addcontentsline{toc}{section}{#1}
        \noindent
        \stylesection{#1}
        \tl_if_novalue:nTF {#4} {} {\quad\hyperref[#4]{\déemph{\small voir~#4~→}}}
        \medskip
    \end{paracol}
    \nopagebreak[4]
}

\DeclareDocumentCommand \soussectioncv { m m } {
    \medskip
    \setcolumnwidth{\largeurmarge}
    \begin{paracol}{2}
        \noindent
        \includegraphics[keepaspectratio, height=0.6\baselineskip]{#2.png}
        \hspace{\longueurintercolonne}
        {\color{\couleurprincipale!20!white}\ligne[0.1\baselineskip]{3pt}}
        \switchcolumn
        \phantomsection
        \addcontentsline{toc}{subsection}{#1}
        \noindent
        \stylesoussection{#1}
        \medskip
    \end{paracol}
    \nopagebreak[4]
}

\DeclareDocumentCommand \expérience { m m m m m m m o o } {
    \setcolumnwidth{\largeurmarge, \fill}
    \setlength{\columnsep}{\longueurintercolonne}
    \tl_if_novalue:nTF {#8} {} {\phantomsection\label{#8}{}}
    \begin{paracol}{2}
        \raggedleft
        #1
        \switchcolumn
        \justifying\noindent
        \textbf{#2},~#3,~\emph{#4}
        \tl_if_blank:nTF {#5} {} {,~en~collaboration~avec~#5}
        .
        \tl_if_blank:nTF {#6} {} {\\#6}
        \tl_if_novalue:nTF {#9} {} {\quad\hyperref[#9]{\déemph{\small →}}}
        \tl_if_blank:nTF {#7} {} {\findeligne#7}
    \end{paracol}
    \medskip

}

\DeclareDocumentCommand \accroche { m } {
    \begin{cadreaccroche}
        #1
    \end{cadreaccroche}
    \medskip
}

\DeclareDocumentCommand \cursus { m m m m m m m m } {
    \setcolumnwidth{\largeurmarge, \fill}
    \setlength{\columnsep}{\longueurintercolonne}
    \begin{paracol}{2}
        \raggedleft
        #1
        \switchcolumn
        \justifying\noindent
        \textbf{#2~#3}
        \tl_if_blank:nTF {#4} {} {,\emph{~avec~#4}}
        \tl_if_blank:nTF {#5} {} {,~mention~\textbf{#5}}
        \tl_if_blank:nTF {#6} {} {,~spécialité~\textbf{#6}}
        \tl_if_blank:nTF {#7} {} {,~option~\textbf{#7}}
        ,~#8
    \end{paracol}
    \medskip
}

\DeclareDocumentCommand \compétence { m m m m } {
    \setcolumnwidth{\largeurmarge, \fill, \largeurmarge, \fill}
    \setlength{\columnsep}{\longueurintercolonne}
    \begin{paracol}{4}
        \raggedleft
        \textcolor{\couleurprincipale}{#1}
        \switchcolumn
        \justifying\noindent
        #2
        \switchcolumn
        \raggedleft
        \textcolor{\couleurprincipale}{#3}
        \switchcolumn
        \justifying\noindent
        #4
    \end{paracol}
    \medskip
}

\DeclareDocumentCommand \domaine { O{Domaines} m } {
    \setcolumnwidth{\largeurmarge, \fill}
    \setlength{\columnsep}{\longueurintercolonne}
    \begin{paracol}{2}
        \raggedleft
        \textcolor{\couleurprincipale}{#1}
        \switchcolumn
        \justifying\noindent
        #2
    \end{paracol}
    \medskip
}

\DeclareDocumentCommand \nom { m } {\cs_set:Npn \l_informations_nom {#1}}
\DeclareDocumentCommand \courriel { m } {\cs_set:Npn \l_informations_courriel {#1}}
\DeclareDocumentCommand \téléphone { m } {\cs_set:Npn \l_informations_téléphone {#1}}
\DeclareDocumentCommand \adresse { m } {\cs_set:Npn \l_informations_adresse {#1}}
\DeclareDocumentCommand \gitlab { m } {\cs_set:Npn \l_informations_gitlab {#1}}
\DeclareDocumentCommand \github { m } {\cs_set:Npn \l_informations_github {#1}}
\DeclareDocumentCommand \hal { m } {\cs_set:Npn \l_informations_hal {#1}}
\DeclareDocumentCommand \âge { m } {\cs_set:Npn \l_informations_âge {#1}}
\DeclareDocumentCommand \photo { m } {\cs_set:Npn \l_informations_photo {#1}}
\DeclareDocumentCommand \titre { m } {\cs_set:Npn \l_informations_titre {#1}}

\DeclareDocumentCommand \placertitre {} {
    \noindent
    \begin{minipage}{0.5\textwidth}
        \raggedright
        {\Huge\l_informations_nom}\\
        \medskip
        \textcolor{Gray}{\emph{\nohyphens{\large\l_informations_titre}}}
    \end{minipage}
    \hfill
    \begin{minipage}{0.4\textwidth}
    \begin{minipage}{0.5\textwidth}
        \raggedleft
        \textcolor{Gray}{
        \l_informations_adresse\\
        \l_informations_téléphone\\
        \href{mailto:\l_informations_courriel}{\addfontfeatures{Numbers=Lining, Scale=0.8}\l_informations_courriel}\\
        \l_informations_âge\\
        \href{\l_informations_gitlab}{Gitlab}\enskip
        \href{\l_informations_github}{Github}\enskip
        \href{\l_informations_hal}{HAL}}
    \end{minipage}
    \begin{minipage}{0.5\textwidth}
        \raggedleft
        % \begin{cadrephoto}
        \fbox{
            \includegraphics[keepaspectratio, width=0.8\textwidth]{\l_informations_photo}
        }
        % \end{cadrephoto}
    \end{minipage}
  \end{minipage}
  \newline
  \bigskip
}

\AtBeginDocument{
    \latin
    \nocoloredwordhyphenated % Pour éviter une espace superflue en fin de ligne avant un lien coloré…
    \sloppy % Pour éviter des dépassements de ligne.
}

% \input{report}
